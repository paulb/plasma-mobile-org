---
title:      "Plasma Mobile: weekly update: part 6"
created_at: 2019-11-08 15:50:00 UTC+2
author:     Plasma Mobile team
layout:     post
---

The Plasma Mobile team is happy to present the sixth weekly blogpost. This week's update is full of application updates, which is in line with KDE's goal of [KDE is all about apps](https://kde.org/goals).

## Libraries

### Kirigami

Kirigami's Card component [now uses gradient stops instead of blur](https://commits.kde.org/kirigami/4203a779031cb7c2268fbdd75d3ddddd9d786056), which improves performance on low performance GPUs. The ColumnView component has [improved flicking behavior](https://commits.kde.org/kirigami/ea1eb618d446c82643c2b3ad6e2f883126a6a74d), its flicking gestures should now feel more natural and easier to use.

### Mauikit

The improved SelectionBar component behaves more like most touch applications in selection mode. Now the SelectionBar has a list of actions to perform on the selected files. The SelectionBar can still group files or items across different locations. A list is shown to peek at which files have been selected and allow adjusting the selection.

![Index selection bar](/img/screenshots/index-selection-bar.png){: .blog-post-image-small}

The templated delegates used by Maui apps in list and grid views have been polished even more. Additionally, they are now used in more places.

## Applications

### Phonebook

The instant messaging actions in the phonebook now [ show the display name of the service instead of the protocol](https://invent.kde.org/kde/plasma-phonebook/commit/e89b35d3bffa2bd1007b2054c8ce83e7382b23e1). Its editing mode now fits smaller screens and is displayed correctly. For this a fix has been [landed in Kirigami](https://commits.kde.org/kirigami/839983aa3604435d814c741c59c6dcb485ca33a1).

### qrca

The QR Code scanner [now supports `optauth://` uris](https://invent.kde.org/kde/qrca/commit/966534987211d17884717b28fe1760780c4d3ce8), which will in the future be handled by the Keysmith application. It now also tries to [set the camera to a specific format](https://invent.kde.org/kde/qrca/commit/ff2a2b71e02e44e9386ae87f7e185ec29201989c) to prevent it being unable to decode an unexpected format.

### Okular

Okular's search bars now [work correctly on Plasma Mobile](https://invent.kde.org/kde/okular/merge_requests/56).

### vvave

In vvave tracks can now be quickly appended or removed from the main playlist using dedicated button. It also can save the current playlist to a personal playlist now.

![vvave quick actions](/img/screenshots/vvave-quick-action.png){: .blog-post-image-small}

The new focus view has been improved visually.

![vvave focus view](/img/screenshots/vvave-focus-view.png){: .blog-post-image-small}

vvave can quickly filter tracks if the list is big enough using the templated data models from MauiKit.

![vvave filtering](/img/screenshots/vvave-filtering.png){: .blog-post-image-small}

## Upstream

Ofono phonesim, an application that can be used to develop SMS and telephony applications, now [supports Qt 5](https://git.kernel.org/pub/scm/network/ofono/phonesim.git/commit/?id=f09c37df3fd0da74bf04360e3be465c780613727) upstream.

Currently `plasma-phone-components` and `plasma-nano` repositories are part of playground, recently [they were moved to kdereview](https://marc.info/?l=kde-core-devel&m=157319701027562&w=2), once the review process completes successfully, they will be moved to `kde/workspace` and will be released along with Plasma 5.18.

## Events

Some of us attended the [Qt World Summit 2019](https://www.qt.io/qtws19/home) in Berlin. At our booth we demo'd Plasma Mobile running on Nexus 5X and Purism Librem 5 devkit.

![PlaMo at QtWS 2019](/img/qtws-2019-plamo.jpg){: .blog-post-image-small}

## Want to be part of it?

Next time your name could be here! To find out the right task for you, from promotion to core system development, check out [Find your way in Plasma Mobile](https://www.plasma-mobile.org/findyourway). We are also always happy to welcome new contributors on our [public channels](https://www.plasma-mobile.org/join). See you there!
